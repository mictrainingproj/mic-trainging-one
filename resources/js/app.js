
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('city', require('./components/City.vue').default);
<<<<<<< HEAD
Vue.component('expense', require('./components/Expense.vue').default);
Vue.component('income', require('./components/Income.vue').default);
Vue.component('location', require('./components/Location.vue').default);
=======
Vue.component('course', require('./components/Course.vue').default);
Vue.component('location', require('./components/Location.vue').default);
Vue.component('duration', require('./components/Duration.vue').default);
Vue.component('teacher', require('./components/Teacher.vue').default);
Vue.component('staff', require('./components/Staff.vue').default);
Vue.component('township', require('./components/Township.vue').default);
>>>>>>> 0df24c7528ee149f70e3ce9fa9cc02a4a373c283

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import VueResource from 'vue-resource';
Vue.use(VueResource);


Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr("content");
const app = new Vue({
    el: '#app'
});
